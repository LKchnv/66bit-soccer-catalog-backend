﻿using SoccerPlayersCatalog.Core.Interfaces;

namespace SoccerPlayersCatalog.UseCases
{
    public abstract class AbstractHandler
    {
        protected readonly ISoccerDbContext db;

        public AbstractHandler(ISoccerDbContext context)
        {
            db = context;
        }
    }
}
