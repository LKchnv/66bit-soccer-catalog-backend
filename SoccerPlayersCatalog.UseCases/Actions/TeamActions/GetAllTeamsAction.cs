﻿using MediatR;
using SoccerPlayersCatalog.Core.Models.Dtos;
using SoccerPlayersCatalog.UseCases.Queries.TeamQueries;
using SoccerPlayersCatalog.UseCases.Services.Actions;
using System.Threading.Tasks;

namespace SoccerPlayersCatalog.UseCases.Actions.TeamActions
{
    public class GetAllTeamsAction : AbstractAction
    {
        public GetAllTeamsAction(IMediator mediator) : base(mediator)
        { }

        public async Task<TeamDto[]> GetAllTeams()
        {
            return await mediator.Send(new GetAllTeamsQuery());
        }
    }
}
