﻿using MediatR;
using SoccerPlayersCatalog.Core.Models.Dtos;
using SoccerPlayersCatalog.UseCases.Commands.TeamCommands;
using SoccerPlayersCatalog.UseCases.Services.Actions;
using System.Threading.Tasks;

namespace SoccerPlayersCatalog.UseCases.Actions.TeamActions
{
    public class CreateNewTeamAction : AbstractAction
    {
        public CreateNewTeamAction(IMediator mediator) : base(mediator)
        { }

        public async Task<int> CreateNewTeam(TeamDto team)
        {
            return await mediator.Send(new CreateNewTeamCommand { Title = team.TeamTitle });
        }
    }
}
