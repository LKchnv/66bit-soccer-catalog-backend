﻿using MediatR;
using SoccerPlayersCatalog.Core.Models.Dtos;
using SoccerPlayersCatalog.UseCases.Queries.CountryQueries;
using System.Threading.Tasks;

namespace SoccerPlayersCatalog.UseCases.Services.Actions.CountryActions
{
    public class GetAllCountriesAction : AbstractAction
    {
        public GetAllCountriesAction(IMediator mediator) : base(mediator)
        { }

        public async Task<CountryDto[]> GetAllCountries()
        {
            return await mediator.Send(new GetAllCountriesQuery());
        }
    }
}
