﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using SoccerPlayersCatalog.Core.Models.Dtos;
using SoccerPlayersCatalog.UseCases.Commands.PlayerCommands;
using SoccerPlayersCatalog.UseCases.Services.Actions;
using System.Threading.Tasks;

namespace SoccerPlayersCatalog.UseCases.Actions.PlayerActions
{
    public class UpadatePlayerDataAction : AbstractAction
    {
        public UpadatePlayerDataAction(IMediator mediator) : base(mediator)
        { }

        public async Task<ResultWrapperDto> UpadatePlayerData(int id, JsonPatchDocument<SoccerPlayerDto> patchModel)
        {
            var result = await mediator.Send(new UpadatePlayerCommand { Id = id, PatchModel = patchModel });
            if(!result)
            {
                return new ResultWrapperDto
                {
                    ResultStatus = StatusCodes.Status400BadRequest,
                    ResultContent = new ErrorResponseDto
                    {
                        ErrorTextForUser = "Failed to update player record"
                    }
                };
            }

            return new ResultWrapperDto
            {
                ResultStatus = StatusCodes.Status200OK,
                ResultContent = true
            };
        }
    }
}
