﻿using MediatR;
using SoccerPlayersCatalog.Core.Models.Dtos;
using SoccerPlayersCatalog.UseCases.Queries.PlayerQueries;
using SoccerPlayersCatalog.UseCases.Services.Actions;
using System.Threading.Tasks;

namespace SoccerPlayersCatalog.UseCases.Actions.PlayerActions
{
    public class GetAllPlayersAction : AbstractAction
    {
        public GetAllPlayersAction(IMediator mediator) : base(mediator)
        { }

        public async Task<SoccerPlayerDto[]> GetAllPlayers(int limit, int offset)
        {
            return await mediator.Send(new GetAllPlayersQuery { Limit = limit, Offset = offset });
        }
    }
}
