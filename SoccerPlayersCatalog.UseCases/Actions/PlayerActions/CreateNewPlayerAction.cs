﻿using MediatR;
using Microsoft.AspNetCore.Http;
using SoccerPlayersCatalog.Core.Models.Dtos;
using SoccerPlayersCatalog.UseCases.Commands.PlayerCommands;
using SoccerPlayersCatalog.UseCases.Services.Actions;
using System.Threading.Tasks;

namespace SoccerPlayersCatalog.UseCases.Actions.PlayerActions
{
    public class CreateNewPlayerAction : AbstractAction
    {
        public CreateNewPlayerAction(IMediator mediator) : base(mediator)
        { }

        public async Task<ResultWrapperDto> CreateNewPlayer(SoccerPlayerDto soccerPlayer)
        {
            if(soccerPlayer.Id != null || !string.IsNullOrEmpty(soccerPlayer.TeamTitle) || !string.IsNullOrEmpty(soccerPlayer.CountryTitle))
            {
                return new ResultWrapperDto
                {
                    ResultStatus = StatusCodes.Status400BadRequest,
                    ResultContent = new ErrorResponseDto
                    {
                        ErrorTextForUser = "Validation error"
                    }
                };
            }

            return new ResultWrapperDto
            {
                ResultStatus = StatusCodes.Status200OK,
                ResultContent = await mediator.Send(new CreateNewPlayerCommand { Player = soccerPlayer })
            }; 
        }
    }
}
