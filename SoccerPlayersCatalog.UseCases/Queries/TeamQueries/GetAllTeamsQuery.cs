﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using SoccerPlayersCatalog.Core.Interfaces;
using SoccerPlayersCatalog.Core.Models.Dtos;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SoccerPlayersCatalog.UseCases.Queries.TeamQueries
{
    internal class GetAllTeamsQuery : IRequest<TeamDto[]>
    { }

    internal class GetAllTeamsQueryHandler : AbstractHandler, IRequestHandler<GetAllTeamsQuery, TeamDto[]>
    {
        public GetAllTeamsQueryHandler(ISoccerDbContext context) : base(context)
        { }

        public async Task<TeamDto[]> Handle(GetAllTeamsQuery request, CancellationToken cancellationToken)
        {
            return await db.Teams
                .Select(t => new TeamDto
                {
                    Id = t.Id,
                    TeamTitle = t.TeamTitle
                })
                .ToArrayAsync();
        }
    }
}
