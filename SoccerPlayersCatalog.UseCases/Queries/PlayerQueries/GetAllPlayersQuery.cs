﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SoccerPlayersCatalog.Core.Interfaces;
using SoccerPlayersCatalog.Core.Models.Dtos;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SoccerPlayersCatalog.UseCases.Queries.PlayerQueries
{
    internal class GetAllPlayersQuery : IRequest<SoccerPlayerDto[]>
    {
        public int Limit { get; set; }
        public int Offset { get; set; }
    }

    internal class GetAllPlayersQueryHandler : AbstractHandler, IRequestHandler<GetAllPlayersQuery, SoccerPlayerDto[]>
    {
        private readonly IMapper mapper;

        public GetAllPlayersQueryHandler(ISoccerDbContext context, IMapper mapper) : base(context)
        {
            this.mapper = mapper;
        }

        public async Task<SoccerPlayerDto[]> Handle(GetAllPlayersQuery request, CancellationToken cancellationToken)
        {
            return await db.Players
                .Skip(request.Offset)
                .Take(request.Limit)
                .Include(p => p.Country)
                .Include(p => p.Team)
                .ProjectTo<SoccerPlayerDto>(mapper.ConfigurationProvider)
                .ToArrayAsync();
        }
    }
}
