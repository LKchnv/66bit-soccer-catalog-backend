﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using SoccerPlayersCatalog.Core.Interfaces;
using SoccerPlayersCatalog.Core.Models.Dtos;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SoccerPlayersCatalog.UseCases.Queries.CountryQueries
{
    internal class GetAllCountriesQuery : IRequest<CountryDto[]>
    { }

    internal class GetAllCountriesQueryHandler : AbstractHandler, IRequestHandler<GetAllCountriesQuery, CountryDto[]>
    {
        public GetAllCountriesQueryHandler(ISoccerDbContext context) : base(context)
        { }

        public async Task<CountryDto[]> Handle(GetAllCountriesQuery request, CancellationToken cancellationToken)
        {
            return await db.Countries.Select(c => new CountryDto
            {
                Id = c.Id,
                CountryName = c.CountryName
            }).ToArrayAsync();
        }
    }
}
