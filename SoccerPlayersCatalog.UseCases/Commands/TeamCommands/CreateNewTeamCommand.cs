﻿using MediatR;
using SoccerPlayersCatalog.Core.Interfaces;
using SoccerPlayersCatalog.Core.Models.DbModels;
using SoccerPlayersCatalog.Core.Models.Dtos;
using System.Threading;
using System.Threading.Tasks;

namespace SoccerPlayersCatalog.UseCases.Commands.TeamCommands
{
    internal class CreateNewTeamCommand : IRequest<int>
    {
        public string Title { get; set; }
    }

    internal class CreateNewTeamCommandHandler : AbstractHandler, IRequestHandler<CreateNewTeamCommand, int>
    {
        public CreateNewTeamCommandHandler(ISoccerDbContext context) : base(context)
        { }

        public async Task<int> Handle(CreateNewTeamCommand request, CancellationToken cancellationToken)
        {
            var toAdd = new Team
            {
                TeamTitle = request.Title
            };

            await db.AddAsync(toAdd);
            await db.SaveChangesAsync();
            return toAdd.Id;
        }
    }
}
