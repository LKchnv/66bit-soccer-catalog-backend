﻿using AutoMapper;
using MediatR;
using SoccerPlayersCatalog.Core.Interfaces;
using SoccerPlayersCatalog.Core.Models.DbModels;
using SoccerPlayersCatalog.Core.Models.Dtos;
using System.Threading;
using System.Threading.Tasks;

namespace SoccerPlayersCatalog.UseCases.Commands.PlayerCommands
{
    internal class CreateNewPlayerCommand : IRequest<int>
    {
        public SoccerPlayerDto Player { get; set; }
    }

    internal class CreateNewPlayerCommandHandler : AbstractHandler, IRequestHandler<CreateNewPlayerCommand, int>
    {
        private readonly IMapper mapper;

        public CreateNewPlayerCommandHandler(ISoccerDbContext context, IMapper mapper) : base(context)
        {
            this.mapper = mapper;
        }

        public async Task<int> Handle(CreateNewPlayerCommand request, CancellationToken cancellationToken)
        {
            var toAdd = mapper.Map<Player>(request.Player);

            await db.AddAsync(toAdd);
            await db.SaveChangesAsync();
            return toAdd.Id;
        }
    }
}
