﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.JsonPatch;
using SoccerPlayersCatalog.Core.Interfaces;
using SoccerPlayersCatalog.Core.Models.Dtos;
using System.Threading;
using System.Threading.Tasks;

namespace SoccerPlayersCatalog.UseCases.Commands.PlayerCommands
{
    public class UpadatePlayerCommand : IRequest<bool>
    {
        public int Id { get; set; }
        public JsonPatchDocument<SoccerPlayerDto> PatchModel { get; set; }
    }

    public class UpadatePlayerCommandHandler : AbstractHandler, IRequestHandler<UpadatePlayerCommand, bool>
    {
        private readonly IMapper mapper;

        public UpadatePlayerCommandHandler(ISoccerDbContext context, IMapper mapper) : base(context)
        {
            this.mapper = mapper;
        }

        public async Task<bool> Handle(UpadatePlayerCommand request, CancellationToken cancellationToken)
        {
            var existingEntity = await db.Players.FindAsync(request.Id);
            if(existingEntity == null)
            {
                return false;
            }

            var dtoToPatch = mapper.Map<SoccerPlayerDto>(existingEntity);
            request.PatchModel.ApplyTo(dtoToPatch);
            mapper.Map(dtoToPatch, existingEntity);

            await db.SaveChangesAsync();
            return true;
        }
    }
}
