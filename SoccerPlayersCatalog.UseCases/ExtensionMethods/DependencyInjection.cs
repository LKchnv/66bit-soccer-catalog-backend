﻿using Microsoft.Extensions.DependencyInjection;
using SoccerPlayersCatalog.UseCases.Actions.PlayerActions;
using SoccerPlayersCatalog.UseCases.Actions.TeamActions;
using SoccerPlayersCatalog.UseCases.Commands.PlayerCommands;
using SoccerPlayersCatalog.UseCases.Commands.TeamCommands;
using SoccerPlayersCatalog.UseCases.Services.Actions.CountryActions;

namespace SoccerPlayersCatalog.UseCases.ExtensionMethods
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddUseCases
            (this IServiceCollection services)
        {
            services.AddScoped<GetAllCountriesAction>();
            services.AddScoped<CreateNewPlayerAction>();
            services.AddScoped<GetAllPlayersAction>();
            services.AddScoped<UpadatePlayerDataAction>();
            services.AddScoped<CreateNewTeamAction>();
            services.AddScoped<GetAllTeamsAction>();

            return services;
        }
    }
}
