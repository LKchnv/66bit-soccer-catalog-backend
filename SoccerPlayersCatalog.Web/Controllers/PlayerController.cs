﻿using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using SoccerPlayersCatalog.Core.Models.Dtos;
using SoccerPlayersCatalog.Core.Models.RequestModels;
using SoccerPlayersCatalog.UseCases.Actions.PlayerActions;
using System.Threading.Tasks;

namespace SoccerPlayersCatalog.Web.Controllers
{
    [Route("/api/v1/[controller]")]
    public class PlayerController : ControllerBase
    {
        public PlayerController() { }

        [HttpGet("")]
        public async Task<IActionResult> GetAllPlayers
            ([FromServices] GetAllPlayersAction getAllPlayersAction, 
            [FromQuery] PaginationParameters paginationParameters)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok(await getAllPlayersAction.GetAllPlayers(paginationParameters.Limit, paginationParameters.Offset));
        }

        [HttpPost("")]
        public async Task<IActionResult> AddNewPlayer
            ([FromServices] CreateNewPlayerAction createNewPlayerAction, 
            [FromBody] SoccerPlayerDto playerDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await createNewPlayerAction.CreateNewPlayer(playerDto);

            Response.StatusCode = result.ResultStatus;
            return new JsonResult(result.ResultContent);
        }

        [HttpPatch("{playerId:int}")]
        public async Task<IActionResult> UpadatePlayerData
            ([FromServices] UpadatePlayerDataAction upadatePlayerDataAction, 
            [FromRoute] int playerId, 
            [FromBody] JsonPatchDocument<SoccerPlayerDto> patchDocument)
        {
            var result = await upadatePlayerDataAction.UpadatePlayerData(playerId, patchDocument);

            Response.StatusCode = result.ResultStatus;
            return new JsonResult(result.ResultContent);
        }
    }
}
