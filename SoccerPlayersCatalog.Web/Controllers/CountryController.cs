﻿using Microsoft.AspNetCore.Mvc;
using SoccerPlayersCatalog.UseCases.Services.Actions.CountryActions;
using System.Threading.Tasks;

namespace SoccerPlayersCatalog.Web.Controllers
{
    [Route("/api/v1/[controller]")]
    public class CountryController : ControllerBase
    {
        public CountryController() { }

        [HttpGet("")]
        public async Task<IActionResult> GetAllCountries([FromServices] GetAllCountriesAction action)
        {
            return Ok(await action.GetAllCountries());
        }
    }
}
