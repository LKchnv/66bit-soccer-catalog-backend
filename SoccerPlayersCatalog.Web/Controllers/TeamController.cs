﻿using Microsoft.AspNetCore.Mvc;
using SoccerPlayersCatalog.Core.Models.Dtos;
using SoccerPlayersCatalog.Core.Models.RequestModels;
using SoccerPlayersCatalog.UseCases.Actions.TeamActions;
using System.Threading.Tasks;

namespace SoccerPlayersCatalog.Web.Controllers
{
    [Route("/api/v1/[controller]")]
    public class TeamController : ControllerBase
    {
        public TeamController() { }

        [HttpGet("")]
        public async Task<IActionResult> GetAllTeams
            ([FromServices] GetAllTeamsAction getAllTeamsAction)
        {
            return Ok(await getAllTeamsAction.GetAllTeams());
        }

        [HttpPost("")]
        public async Task<IActionResult> AddNewTeam
            ([FromServices] CreateNewTeamAction createNewTeamAction,
            [FromBody] TeamDto teamDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok(await createNewTeamAction.CreateNewTeam(teamDto));
        }
    }
}
