using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using SoccerPlayersCatalog.Core.ExtensionMethods;
using SoccerPlayersCatalog.Core.Interfaces;
using SoccerPlayersCatalog.Core.Models.MappingProfiles;
using SoccerPlayersCatalog.Core.Services;
using SoccerPlayersCatalog.Infrastructure.EfDbContexts;
using SoccerPlayersCatalog.UseCases;
using SoccerPlayersCatalog.UseCases.ExtensionMethods;
using SoccerPlayersCatalog.UseCases.Services.Actions.CountryActions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoccerPlayersCatalog.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(o => o.AddPolicy("AllowAllPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));

            services.AddControllers()
                .AddNewtonsoftJson();

            services.AddSpaStaticFiles(config =>
            {
                config.RootPath = "dist";
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "SoccerPlayersCatalog.Web", Version = "v1" });
            });

            services.AddMediatR(cfg => cfg.RegisterServicesFromAssemblyContaining<AbstractHandler>());
            services.AddAutoMapper(typeof(AutomapperPing));

            var connStr = Environment.GetEnvironmentVariable("__SOCCER_CATALOG_CONN_STR__");

            if (string.IsNullOrEmpty(connStr))
            {
                connStr = Configuration.GetConnectionString("SoccerCatalogConnectionString");
            }

            services.AddUseCases();
            services.AddCoreServices();

            services.AddSingleton(provider => new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new PlayerMappingProfile(provider.GetService<DateTimeHandlerService>()));
            }).CreateMapper());

            services.AddDbContext<SoccerPlayersCatalogContext>(
                options => {
                    options
                    .UseNpgsql(connStr);
                });

            services.AddScoped<ISoccerDbContext>(provider =>
                provider.GetService<SoccerPlayersCatalogContext>());
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "SoccerPlayersCatalog.Web v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors("AllowAllPolicy");

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSpaStaticFiles();
            app.UseSpa(builder => { });
        }
    }
}
