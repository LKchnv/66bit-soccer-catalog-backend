﻿using Microsoft.Extensions.DependencyInjection;
using SoccerPlayersCatalog.Core.Services;

namespace SoccerPlayersCatalog.Core.ExtensionMethods
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddCoreServices
            (this IServiceCollection services)
        {
            services.AddSingleton<DateTimeHandlerService>();

            return services;
        }
    }
}
