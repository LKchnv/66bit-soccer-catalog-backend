﻿using System.ComponentModel.DataAnnotations;

namespace SoccerPlayersCatalog.Core.Models.Dtos
{
    public record TeamDto
    {
        public int Id { get; set; }
        [Required]
        public string TeamTitle { get; set; }
    }
}
