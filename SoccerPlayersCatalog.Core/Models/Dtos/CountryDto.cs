﻿namespace SoccerPlayersCatalog.Core.Models.Dtos
{
    public record CountryDto
    {
        public int Id { get; set; }
        public string CountryName { get; set; }
    }
}
