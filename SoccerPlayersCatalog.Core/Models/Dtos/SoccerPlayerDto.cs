﻿using SoccerPlayersCatalog.Core.Services.JsonConverters;
using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace SoccerPlayersCatalog.Core.Models.Dtos
{
    public record SoccerPlayerDto
    {
        public int? Id { get; set; }
        [Required]
        public string Firstname { get; set; }
        [Required]
        public string Surname { get; set; }
        [Required]
        public string Gender { get; set; }
        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime DateOfBirth { get; set; }
        [Required]
        public int TeamId { get; set; }
        public string TeamTitle { get; set; }
        [Required]
        public int CountryId { get; set; }
        public string CountryTitle { get; set; }
    }
}
