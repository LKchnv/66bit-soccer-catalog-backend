﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoccerPlayersCatalog.Core.Models.DbModels
{
    public partial class Player
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Surname { get; set; }
        public string Gender { get; set; }
        public long? DateOfBirth { get; set; }
        public int TeamId { get; set; }
        public int CountryId { get; set; }

        public virtual Country Country { get; set; }
        public virtual Team Team { get; set; }
    }
}
