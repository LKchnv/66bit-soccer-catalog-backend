﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SoccerPlayersCatalog.Core.Models.DbModels
{
    public partial class Team
    {
        public Team()
        {
            Players = new HashSet<Player>();
        }

        public int Id { get; set; }
        public string TeamTitle { get; set; }

        public virtual ICollection<Player> Players { get; set; }
    }
}
