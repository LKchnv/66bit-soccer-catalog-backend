﻿using AutoMapper;
using SoccerPlayersCatalog.Core.Models.DbModels;
using SoccerPlayersCatalog.Core.Models.Dtos;
using SoccerPlayersCatalog.Core.Services;
using System;

namespace SoccerPlayersCatalog.Core.Models.MappingProfiles
{
    public class PlayerMappingProfile : Profile
    {
        public PlayerMappingProfile(DateTimeHandlerService dateTimeHandler)
        {
            CreateMap<Player, SoccerPlayerDto>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Firstname, opt => opt.MapFrom(src => src.Firstname))
                .ForMember(dest => dest.Surname, opt => opt.MapFrom(src => src.Surname))
                .ForMember(dest => dest.Gender, opt => opt.MapFrom(src => src.Gender))
                .ForMember(dest => dest.DateOfBirth, opt => opt.MapFrom(src => src.DateOfBirth != null ? dateTimeHandler.ConvertDbTimestampToDate(src.DateOfBirth.Value) : DateTime.MinValue))
                .ForMember(dest => dest.TeamId, opt => opt.MapFrom(src => src.TeamId))
                .ForMember(dest => dest.TeamTitle, opt => opt.MapFrom(src => src.Team!= null ? src.Team.TeamTitle : null))
                .ForMember(dest => dest.CountryTitle, opt => opt.MapFrom(src => src.Country!= null ? src.Country.CountryName : null))
                .ForMember(dest => dest.CountryId, opt => opt.MapFrom(src => src.CountryId));

            CreateMap<SoccerPlayerDto, Player>()
               .ForMember(dest => dest.Id, opt => opt.Ignore())
               .ForMember(dest => dest.Firstname, opt => opt.MapFrom(src => src.Firstname))
               .ForMember(dest => dest.Surname, opt => opt.MapFrom(src => src.Surname))
               .ForMember(dest => dest.Gender, opt => opt.MapFrom(src => src.Gender))
               .ForMember(dest => dest.DateOfBirth, opt => opt.MapFrom(src => dateTimeHandler.ConvertDateToDbTimestamp(src.DateOfBirth)))
               .ForMember(dest => dest.TeamId, opt => opt.MapFrom(src => src.TeamId))
               .ForMember(dest => dest.CountryId, opt => opt.MapFrom(src => src.CountryId));
        }
    }
}
