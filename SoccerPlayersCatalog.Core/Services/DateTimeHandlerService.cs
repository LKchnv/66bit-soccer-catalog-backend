﻿using System;

namespace SoccerPlayersCatalog.Core.Services
{
    public class DateTimeHandlerService
    {
        public DateTimeHandlerService() { }

        public DateTime ConvertDbTimestampToDate(long utcDate)
        {
            var dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dateTime = dateTime.AddSeconds(utcDate);
            return DateTime.SpecifyKind(dateTime, DateTimeKind.Local);
        }

        public long ConvertDateToDbTimestamp(DateTime localDate)
        {
            var currentDateTimeUtc = DateTime.SpecifyKind(localDate.Date, DateTimeKind.Utc);
            return ((DateTimeOffset)currentDateTimeUtc).ToUnixTimeSeconds();
        }
    }
}
